from .action import Action3
from mud.events import BreakIntoEvent

class BreakIntoAction(Action3):
	EVENT = BreakIntoEvent
	RESOLVE_OBJECT = "resolve_for_operate"
	RESOLVE_OBJECT2 = "resolve_for_use"
	ACTION = "break-into"
